#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  7 20:20:04 2016

@author: azophy
"""

from PySide.QtCore import *
from PySide.QtGui import *
from models import *
from views.MainDialog import MainDialog


# =======================
#      MAIN PROGRAM
# =======================

# Main Program
if __name__ == "__main__":
    import sys

    # Create an instance of the application window and run it
    qt_app = QApplication(sys.argv)
    app = MainDialog()
    sys.exit(app.exec_())
