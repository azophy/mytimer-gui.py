#! /usr/bin/env python
# -*- coding: utf-8 -*-

from PySide.QtCore import *
from PySide.QtGui import *
from models.Logger import Logger

class LogSummary(QWidget):
    ''' Read raw log using logger '''

    def __init__(self, parent=None):
        # Initialize the object as a QWidget and
        # set its title and minimum width
        super(LogSummary, self).__init__(parent)
        #QWidget.__init__(self)
        self.logger = Logger()
        self.errors =[]
        self.draw_layout()

    def draw_layout(self):
        self.logListBox = QListWidget()
        self.update_summary()

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.logListBox)
        self.setLayout(self.layout)

    def update_summary(self):
        # Load the log
        logs = self.logger.summary_log()
        dates = logs.keys()
        dates.sort(reverse=True)

        display = []
        for date in dates:
            display.append('-------- ' + date + ' --------')
            for act in logs[date]:
              display.append(act + " \t " + self.logger.display_time(logs[date][act])) #, 'minutes'
            display.append('')

        self.logListBox.insertItems(0, display)

    def run(self):
        # Show the form
        self.show()
        # Run the qt application
        qt_app.exec_()
