#! /usr/bin/env python
# -*- coding: utf-8 -*-

from PySide.QtCore import *
from PySide.QtGui import *
from models.Logger import Logger

class RawLog(QWidget):
    ''' Read raw log using logger '''

    def __init__(self, parent=None):
        # Initialize the object as a QWidget and
        # set its title and minimum width
        super(RawLog, self).__init__(parent)
        #QWidget.__init__(self)
        self.logger = Logger()
        self.errors =[]
        self.draw_layout()

    def draw_layout(self):
        self.logListBox = QListWidget()
        self.update_log()

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.logListBox)
        self.setLayout(self.layout)

    def update_log(self):
        # Load the log
        display = []
        logs = self.logger.raw_log()
        dates = logs.keys()
        dates.sort(reverse=True)
        for date in dates:
            display.append(date + ' - ' + str(logs[date]))

        self.logListBox.insertItems(0, display)
        
    def run(self):
        # Show the form
        self.show()
        # Run the qt application
        qt_app.exec_()
