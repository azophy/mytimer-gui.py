#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  7 20:20:04 2016

@author: azophy
"""

import sys
from PySide.QtCore import *
from PySide.QtGui import *
from MainTimer import MainTimer
from RawLog import RawLog
from LogSummary import LogSummary

class MainDialog(QDialog):
    def __init__(self, parent = None):
        super(MainDialog, self).__init__(parent)

        self.maintimer = MainTimer()
        self.rawlog = RawLog()
        self.logsummary = LogSummary()

        tabWidget = QTabWidget()
        tabWidget.addTab(self.maintimer, "Timer")
        tabWidget.addTab(self.rawlog, "Raw Log")
        tabWidget.addTab(self.logsummary, "Summary")

        buttonBox = QDialogButtonBox(QDialogButtonBox.Close)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(tabWidget)
        mainLayout.addWidget(buttonBox)
        self.setLayout(mainLayout)

        # Main layout placement
        self.setWindowTitle('My Timer by Azophy')
        self.setMinimumWidth(600)

        # Aditional event delegation
        self.maintimer.timer.timeup.connect(self.update_summary)

    @Slot()
    def update_summary(self):
        self.logsummary.update_summary()
