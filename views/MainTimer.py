#! /usr/bin/env python
# -*- coding: utf-8 -*-

from PySide.QtCore import *
from PySide.QtGui import *
from Tkinter import *
from tkMessageBox import *
# from models.Logger import Logger
# from models.Mytimer import Mytimer
from models.Mytimer import Mytimer
from models.Logger import Logger

class MainTimer(QWidget):
    ''' An example of PySide absolute positioning; the main window
        inherits from QWidget, a convenient widget for an empty window. '''

    def __init__(self, parent=None):
        # Initialize the object as a QWidget and
        # set its title and minimum width
        super(MainTimer, self).__init__(parent)
        #QWidget.__init__(self)
        self.timer = Mytimer()
        self.logger = Logger()
        self.errors =[]
        self.draw_layout()

    def draw_layout(self):
        # The timer config
        self.activity = QLineEdit(self)
        self.duration = QLineEdit(self)
        self.start_button = QPushButton('&Start', self)
        self.pause_button = QPushButton('&Pause', self)
        self.stop_button = QPushButton('&Stop', self)
        self.indicator = QLabel('', self)

        self.form_layout = QFormLayout()
        self.form_layout.addRow('&Activity:', self.activity)
        self.form_layout.addRow('&Duration (minute):', self.duration)
        self.form_layout.addRow('Timer:', self.indicator)

        self.button_box = QHBoxLayout()
        self.button_box.addStretch(1)
        self.button_box.addWidget(self.start_button)
        self.button_box.addWidget(self.pause_button)
        self.button_box.addWidget(self.stop_button)

        # main layout setting
        self.layout = QVBoxLayout()
        self.layout.addLayout(self.form_layout)
        self.layout.addStretch(1)
        self.layout.addLayout(self.button_box)
        self.setLayout(self.layout)

        # Event delegation
        self.timer.timechange.connect(self.time_change)
        self.timer.timeup.connect(self.time_out)
        self.start_button.clicked.connect(self.start_click)
        self.pause_button.clicked.connect(self.pause_click)
        self.stop_button.clicked.connect(self.stop_click)

    @Slot()
    def start_click(self):
        if not self.validate():
            QMessageBox.information(self, 'Error!', self.errors[0]),
            return

        self.timer.duration = int(self.duration.text())*60
        self.timer.activity = self.activity.text()

        if not self.timer.validate():
            QMessageBox.information(self, 'Error!', self.timer.errors[0]),
            return

        self.timer.start()

    @Slot()
    def pause_click(self):
        self.timer.pause()

    @Slot()
    def stop_click(self):
        self.timer.stop()

    @Slot()
    def time_change(self):
        txt = ('%d:%d remaining' %
            (self.timer.remaining/60, self.timer.remaining%60))
        self.indicator.setText(txt)
        self.indicator.repaint()
        #print txt, 'remaining'

    @Slot()
    def time_out(self):
        print 'ok'
        # res = QMessageBox.information(self, 'MyTimer', 'Time is Up!')
        # self.activateWindow()
        showinfo('Ok', 'TIMES UP')
        self.logger.write_log(self.timer)

    # Validation & Error handling methods
    def validate(self):
        self.errors[:] = [] # empty the errors list

        if not self.activity.text():
            self.errors.append('Activity cannot be empty')
        if not self.duration.text():
            self.errors.append('Duration cannot be empty')

        return not self.has_error()

    def has_error(self):
        return len(self.errors)

    def run(self):
        # Show the form
        self.show()
        self.time_change()
        # Run the qt application
        qt_app.exec_()
