#! /usr/bin/env python

''' A simple python-based timer along with some aditional features:
- log activity into simple anydbm database (defined by MYTIMER_FILE environment variable)
- display popup when time is up

This simple program was created as my way to practice my Python 2.7
programming skill.

By : Abdurrahman Shofy Adianto <azophy@gmail.com>
License: WTFPL
'''
import time
from PySide.QtCore import *

class Mytimer(QObject):
    timeup = Signal()
    timechange = Signal()

    def __init__(self):
      # Initialize the PunchingBag as a QObject
      QObject.__init__(self)
      self.activity = ''
      self.duration = 0 # in seconds
      self.remaining = 0 # in seconds
      self.is_running = False
      self.errors = []

      self.timer = QTimer(self)
      self.timer.timeout.connect(self.qtimer_update)
      self.timer.start(1000)

    def start(self):
      if self.remaining <= 0:
          self.remaining = self.duration
      self.is_running = True
      self.timechange.emit()

    def pause(self):
      self.is_running = False
      self.timechange.emit()

    def stop(self):
      self.is_running = False
      self.remaining = 0
      self.timechange.emit()

    @Slot()
    def qtimer_update(self):
        if self.is_running:
            if self.remaining <= 0:
                self.is_running = False
                self.timeup.emit()
            else:
                self.remaining -= 1
                self.timechange.emit()

    # Validation & Error handling methods
    def validate(self):
        self.errors[:] = [] # empty the errors list
        if not self.activity:
            self.errors.append('Activity cannot be empty')
        if not self.duration:
            self.errors.append('Duration must be > 0')

        return not self.has_error()

    def has_error(self):
        return len(self.errors)
