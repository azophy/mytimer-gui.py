import time
import shelve
import os
import copy

class Logger(object):

    def __init__(self):
      # Initialize the PunchingBag as a QObject
      self.filename = os.environ['MYTIMER_FILE']
      self.time_format = "%d/%m/%Y %H:%M:%S"

    def write_log(self, timer):
      now = time.strftime(self.time_format)

      o = { 'activity': timer.activity, 'duration': timer.duration/60 }
      db = shelve.open(self.filename,'c')
      db[now] = o
      db.close()

    def raw_log(self):
      db = shelve.open(self.filename, 'c')

    #   keys = db.keys()
    #   keys.sort()
    #   for k in keys:
    #     date = k[:10]
    #     if date not in summary:
    #       summary[date] = {}
    #     if db[k]['activity'] not in summary[date]:
    #       summary[date][db[k]['activity']] = int(db[k]['duration'])
    #     else:
    #       summary[date][db[k]['activity']] += int(db[k]['duration'])
      result = dict(db)
      db.close()
      return result

    def summary_log(self):
      db = self.raw_log()
      summary = {}

      for k in db.keys():
        date = k[:10]
        if date not in summary:
          summary[date] = {}
        if db[k]['activity'] not in summary[date]:
          summary[date][db[k]['activity']] = int(db[k]['duration'])
        else:
          summary[date][db[k]['activity']] += int(db[k]['duration'])

      return summary

    def display_time(self,minutes):
      return str(minutes/60) + ' hours ' + str(minutes%60) + ' minutes'
