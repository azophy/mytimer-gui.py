MYTIMER-GUI
==========

A simple GUI based work timer with logging features.
Created as an exercise for my python 2 programming skill.

Created by: Abdurrahman Shofy Adianto <azophy@gmail.com>
License: WTFPL <www.wtfpl.net>

INSTALL
-------
pip -r requirements.txt
./main.py
